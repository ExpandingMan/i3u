# `i3u`

[![Oh My Fish Framework](https://img.shields.io/badge/Oh%20My%20Fish-Framework-007EC7.svg?style=flat-square)](https://www.github.com/oh-my-fish/oh-my-fish)

A set of useful command line utilities that should have been included with `i3` itself.

Some of these are a major refactoring of [`i3ass`](https://github.com/budlabs/i3ass) but with `fish`
instead of `bash`, `jq` instead of `awk` and a less disgusting logo.


## Install

```fish
$ omf install i3u
```

### Dependencies
The following tools are required for the utilities in this plugin to run without error.
- `jq`
- `xdotool`

These are commonly available in package managers, for example `pacman -S jq xdotool` or `apt install
jq xdotool`.
